#ifndef GAME_HPP
#define GAME_HPP

#include "bank.hpp"
#include "board.hpp"
#include "player.hpp"
#include "bot.hpp"
#include "rectangle_with_text.hpp"
#include "button.hpp"
#include "field.hpp"
#include "card_chance_community.hpp"
#include "rectangle_with_text.hpp"
#include "my_pixmap_graphics_item.hpp"
#include "notification.hpp"

#include <vector>
#include <queue>
#include <deque>
#include <cstdlib>
#include <algorithm>
#include <iostream>

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QApplication>
#include <QObject>

#include "game_over.h"

class Game:public QObject{
Q_OBJECT
public:
    Game(Board *b,QString name, QString token);
     ~Game();

    //prosledjuje scenu,crta tablu i user deo i stavlja patkicu spremnu za animaciju
    void initialize_board(QGraphicsScene *scene_table);

    //Function for the bottom right part of the screen
    void display_user_info();

    Board *getBoard() const {return board_;}

    void init_game();
    QString notification_string_;
    Notification* bot1_about = new Notification(1055,10,"Player Bot 1");
    Notification* bot2_about = new Notification(1055,170,"Player Bot 2");


    Notification* bot3_about = new Notification(1055, 330,"Player Bot 3");

    bool pocetak_igre=true;
    QString token_human_;

    //void set_chance_community_card_shown(bool value);

signals:
    // stao je na field_kind koji je property/supply/railway a koji poseduje banka
    void stepped_on_property();
    //stao je na field_kind koji je taksa
    void stepped_on_tax();
    //stao je na field_kind koji je go to jail
    void stepped_on_go_to_jail();


    void stepped_on_community_field();

    //koristi se u buy_property tu kad instancira rect with text pozove se slot u kome se ovo iscrta
    //ujedno u tom slotu sto se pozove se hvataju signali za prikazivanje kartica na tabli na mouse press a na mouse release nestaju
    void add_to_owned_properties(QString);

    // SIGNAL ZA PRODAJU PROPERTIES, TREBA DA SE CONNECT SA SLOTOM remove_sold_property  U KLASI GAME, SLICNO KAO ZA MORTGAGE
    void sell_property_and_remove_from_owned_properties(QString);

    //----------------------------------------------------------------------------------
    //kada je igrac u zatvoru a POSEDUJE karticu za izlazak iz zatvora
    void player_owns_jail_card();
   //kada je igrac u zatvoru a NE POSEDUJE karticu za izlazak iz zatvora
    void player_doesnt_own_jail_card();
   //jer mora pre animacije kockica i tokena da se korisniku stavi do znanja da je u zatvoru i sta zeli da uradi povodom toga
   void processing_about_jail_finished();
   //kada se ovo emit znaci sve zavrseno u izlazenjiz zatvora i moze da se pokrene animacija kockice + token
   void processing_about_jail_with_work_finished();
   //zavrsio sa fjom mortgage property
   void finished_with_mortgaging_property(int);
   //zavrsio sa fjom gde placa izlazak iz zatvora nakon sto je stavio field pod hipoteku
   void finished_with_paying_after_mortgaging();
   //konacno ovim saljem field u pay_to_getout_of_jail i poziva se slot -- fja iz logike
   void send_field_to_mortgage(Field& field, Player& player);
   //kada bankrotira -- TODO:videti sta cemo da radimo kada se to desi, gde da ga hvatamo
   void player_bankrupted();
   //--------------------------------------------------------------------------------------


    void got_jail_free_card();
    void display_chance_community_card(Card_Chance_Community*);
    //bool tells if the card was previosly shown


    void go_back_three_spaces();

    void change_money_from_a_card_signal();

    void move_by_card();


    //-----------------KADA HOCE DA KUPI PROPERTY A NEMA PARA-------------
    void finished_tell_about_not_having_money();
    //---------------------------------------------------------------------


    //-------------------ZA OBRADU ONIH 5 PITANJA---------------------------
    //----------------------------------------------------------------------
    void do_you_want_to_lift_the_mortgage();
    void enough_money_to_lift_the_mortgage();
    void not_enough_money_to_lift_the_mortgage();

    void stepped_on_previously_bought_field();


    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------

    void stepped_on_another_players_property();
    void bots_is_rolling_the_dice();
    void next_player();
    void human_wants_to_roll();



    void enable_button(QString);
    void hocu_move_player_on_the_board();

public slots:



    void change_current_player();
    void move_player_on_the_board();

    //------------HUMAN IGRA-----------------
    //  fja koja izvrsava turn za human playera
    void play_game();

    void  return_score();

    // Board& get_board();
    //------------BOTOVI IGRAJU-----------------/////////////////////////////////////////////////////////
    void play_game_for_bots();

    //---------KUPOVANJE PROPERTY/SUPPLY/RAILROAD---------------
    //dodaje pitanje da li korisnik zeli da kupi property ili ne na scenu
    void ask_about_buying();
    //poziva fju iz logike -- u pozadini zaista kupi taj property i dodaje u Properties you own kupljeno polje
    void buy_property();
    //samo brise pitanje o kupovini sa scene
    void dont_buy_property();

    //---------PLACANJE TAKSE---------------
    //dodaje pitanje vezano za taksu na scenu
    void ask_about_tax();
    //poziva fju iz logike -- u pozadini se zaista uzima taksa za prvu opciju
    void take_the_tax_option_one();
    //poziva fju iz logike -- u pozadini se zaista uzima taksa za drugu opciju
    void take_the_tax_option_two();

    //------------ODLAZAK U ZATVOR-------------------
    //dodaje tekst koji obavestava korisnika da ide u zatvor i
    //poziva fju iz logike -- u pozadini se zaista obradjuje odlazak u zatvor
    void tell_about_going_to_jail();
    //uklanja tekst o tome da korisnik ide u zatvor zbog narednih pitanja koja dolaze i treba da stoje na tom mestu
    //-------------------------------------------------------------------------------------------------------------------------
    //brise text item koji mu govori da ide u zatvor
    void remove_tell_about_going_to_jail();
    //-------------------------------------------------------------------------------------------------------------------------
    //iscrtava ovaj polje u owned properties koje je player kupio
    void display_in_owned_properties(QString name);
    //kada igrac odabere da mortgage property onda rect u properties you own postaje crn
    void change_color_to_black_when_mortgaged(QString name);
    void activate_jail_free_card_posetion();

     //-----------IZLAZAK IZ ZATVORA------------------
    //postavlja mu sva pitanja oko zatvora i gleda da li ima karticu da izadje ili mora da plati
    void process_player_being_in_jail();
    //dodaje pitanje da li korisnik zeli da iskoristi jail_card ili zeli da plati
    void ask_about_using_jail_card();
    //brise pitanje i dugmad
    void remove_ask_about_jail_card();
    //obradjuje u pozadini slucaj kada je odgovor da zeli da plati iako ima karticu jail_card
    //ili slucaj kada nema karticu i mora da plati da bi izasao
    void pay_to_get_out_of_jail();
    //samo pozove ovu iznad
    void pay_even_with_card();
    //u pozadini(logici) obradjuje situaciju kada korisnik odabere da iskoristi karticu jail_card kako
    //bi izasao iz zatvora
    void use_card_to_get_out_of_jail();
    //da ocisti sve iz notifikacija humana nakon pay_to_get_out_of_jail
    void remove_pay_to_get_out_jail();
    //kada ne mortgaguje -- jer tada ide bankrotiranje
    void remove_not_mortgaging();
    //da prosledim naziv fielda koji se stavlja pod hipoteku iz fje buy_property
    void got_mortgage_to_get_out_of_jail(QString);
    //fja u kojoj placa nakon sto je stavio hipoteku
    void pay_after_mortgaging(int);


    //----------------ZA CHANCE I COMMUNITY---------------------------
    void add_chance_community_card_to_the_scene(Card_Chance_Community *c) {board_->scene_->addItem(c);}
    void remove_chance_community_card_from_scene();
    //------------------------------------------------------------------
    void remove_tell_about_not_having_money();
    void remove_mortgage_question();

    void mortgage_property(Field& field, Player& player);

    //-------------------ZA OBRADU ONIH 5 PITANJA---------------------------
    //----------------------------------------------------------------------
    void ask_about_lifting_mortgage();

    void lifting_mortgage();
    void not_lifting_mortgage();

    void remove_notif_about_lifting();

    void ask_about_previously_bought_field();

    void remove_ask_about_previously_bought_field();

    void do_the_mortgaging_on_current_field();

    //BRISANJE U PROPERTIES YOU OWN KADA HOCE DA PRODA
    // SLOT ZA BRISANJE POLJA IZ PROPERTIES YOU OWN KAD SE PRODA
    void sell_property();
    void remove_property_from_owned_properties(QString name);

    void set_top_notif_and_okay(QString);
    void remove_top_notif_and_okay();

    void build_estate();

    void ask_about_selling_estate();
    void sell_all_estates();
    void sell_one_estate();
    void remove_ask_about_selling_estates();
    //-----------------------------------------------------------------------
    //-----------------------------------------------------------------------

    void pay_to_other_player();

    void change_budget_rect();

private:
    Bank& bank_ = Bank::return_bank();
    Board *board_;
    int nplayers = 4;
    int chance_community_card_shown_=0;
    //TODO:  DODATI DONJU CRTU ZA OVE MEMBER PROMENLJIVE
    /// Koriste se za izvlacenje karte kada se stane na chance ili community polje
    std::pair<Card_Chance_Community*, bool> chance_jail_card;
    std::pair<Card_Chance_Community*, bool> community_jail_card;

    std::vector<Player*> players;
    std::queue<Player*> order;
    
    //Red za sve karte
    std::deque<Card_Chance_Community*> chance_cards;
    std::deque<Card_Chance_Community*> community_cards;

    std::vector<Button*> buttons_;

private:



    unsigned owner_to_id(const Owner o);
    Owner id_to_owner(unsigned player_id);
    Player* get_player_by_id(const Owner o);
    void return_jail_card(Field_kind card_from);
    void set_jail_card(Field_kind which, bool set, Card_Chance_Community* card);
    int calculate_mortgage(int mortgage) const;
    //void mortgage_property(Field& field, Player& player);
    void be_bankrupt(Player* player);
    //----------------------------------------------------------------------
    void buy_from_bank(Field& curr_field, Player& curr_player);
   // void buy_from_bank(Field& curr_field, Bot& curr_player);
    bool lift_mortgage(Field& curr_field, Player* curr_player);
    void selling_all_estates(Field& curr_field, Player& curr_player);

    void step_on_chance_community_field(Player *curr_player, Field &curr_field);
    void step_on_tax_field(Player* curr_player);
    //----------------------------------------------------------------------
    void step_on_owner_bank(Field& curr_field, Player* curr_player);
 // void step_on_owner_bank(Field& curr_field, Bot& curr_player);
    void step_on_owner_other_player(Field& curr_field, Player* curr_player, Owner owner);
    void mortgage_to_pay_debt(Player* curr_player, int const debt);
    void pay_jail_fee(Player* curr_player);
    void in_jail(Player* curr_player);
    void go_to_jail(Player& curr_player);
    void selling_property(Field& curr_field, Player& player);
    int advance_player(Player& curr_player, int const steps);
    void try_payment_to_bank(Player *curr_player, int money);
    QString regex_match(QString &content, bool letters);
    bool throw_dices_again = true;
    bool play_again = false;
    Player* curr_player_;
    std::vector<Player*> bot_players_;
    Field* curr_field_;
};


#endif // GAME_HPP
