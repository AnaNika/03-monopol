#ifndef CARD_PROPERTY_HPP
#define CARD_PROPERTY_HPP
#include <iostream>
#include <string>

#include <QGraphicsItem>
#include <QString>
#include <QPainter>

#include "field.hpp"

class Card_Property: public QGraphicsItem
{

public:
    Card_Property(QString name,QColor color,int rent_lot,int rent_one_house,int rent_two_houses,int rent_three_houses,
              int rent_four_houses,int rent_hotel,int house_price,int hotel_price,int mortgage,Field_kind kind);

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:

    QString name_;
    QColor color_;
    int rent_lot_;
    int rent_one_house_=-1;
    int rent_two_houses_;
    int rent_three_houses_=0;
    int rent_four_houses_=0;
    int rent_hotel_=-1;
    int house_price_=-1;
    int hotel_price_=-1;
    int mortgage_;
    Field_kind kind_;

};

#endif // CARD_PROPERTY_HPP

