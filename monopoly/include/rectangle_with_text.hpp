#ifndef RECTANGLE_WITH_TEXT_HPP
#define RECTANGLE_WITH_TEXT_HPP
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsScene>
#include <QPainter>
#include <QStyleOption>

#include <iostream>
enum class Button_kind{
    BOUGHT_PROPERTY,
    INTERFACE_BUTTON
};
class Rectangle_with_text :public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    Rectangle_with_text(QString text, QColor color, int x_pos,int y_pos,int width,int height, Button_kind kind=Button_kind::INTERFACE_BUTTON,int id_for_drawing=-100, QGraphicsItem *parent = nullptr);
    Rectangle_with_text(QString text, QColor color,Button_kind kind=Button_kind::INTERFACE_BUTTON, QGraphicsItem *parent = nullptr);
    QRectF boundingRect() const override {return QRectF(x_pos_, y_pos_, width_,height_);}
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;

    void set_text(QString new_text) {text_ = new_text;}
    void set_color(const QColor &color) {color_ = color;}
    QString get_text() const {return text_;}
    int get_id_to_draw() const {return id_for_drawing_;}
    void set_height(int height){height_ = height;}
    void set_width(int width) {width_ = width;}
    void set_y_pos(int y_pos) {y_pos_ = y_pos;}
    void set_x_pos(int x_pos) {x_pos_ = x_pos;}



signals:
    void roll_dice();
    void end_turn();
    void i_want_to_buy_it();
    void i_dont_want_to_buy_it();
    void tax_option_one();
    void tax_option_two();
    void show_card_for_the_owned_property(QString text_);
    void hide_card_for_the_owned_property(QString text_);
    void put_property_under_mortgage(QString);
    void im_not_mortgaging();
    void i_want_to_pay_for_jail();
    void i_want_to_use_the_card();
    void okay_i_dont_have_money_to_buy();
    void i_want_to_lift_the_mortgage();
    void i_dont_want_to_lift_the_mortgage();
    void i_want_to_sell_property();
    void i_want_to_mortgage_this_property();
    void i_want_to_do_nothing();
    void i_want_to_sell_estate();
    void i_want_to_build_house();
    void i_want_to_build_hotel();
    void i_want_to_sell_all_estates();
    void i_want_to_sell_one_estate();

public slots:
    void enable_button(QString);
    void enable_button(){this->blockSignals(false);}
    
private:

    QString text_;
    QColor color_;
    int x_pos_;
    int y_pos_;
    int width_;
    int height_;
    Button_kind kind_;
    int id_for_drawing_;


};
#endif // RECTANGLE_WITH_TEXT_HPP
