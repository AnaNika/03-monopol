#include "../include/rules.h"
#include "../build/ui_rules.h"

Rules::Rules(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Rules)
{
    ui->setupUi(this);
    setWindowFlags(Qt::CustomizeWindowHint);
    setWindowFlags(Qt::Popup);

//    button_click = new QMediaPlayer();
//    button_click->setMedia(QUrl("qrc:/sounds/button_click.wav"));

    connect(ui->pb_back, &QPushButton::clicked, this, &Rules::button_back_clicked);
}

Rules::~Rules() {delete ui;}

void Rules::button_back_clicked()
{
//    button_click_sound();
    delete this;
}

void Rules::button_click_sound() const
{
    if (button_click->state() == QMediaPlayer::PlayingState)
        button_click->setPosition(0);
    else
        button_click->play();
}

void Rules::keyPressEvent(QKeyEvent *event)
{
   if (event->key() == Qt::Key_Escape)
      return;

   QDialog::keyPressEvent(event);
}
