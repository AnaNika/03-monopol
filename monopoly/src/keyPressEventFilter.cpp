#include "../include/keyPressEventFilter.h"

KeyPressEventFilter::KeyPressEventFilter(QObject *parent) : QObject(parent)
{
    inPauseMenu = false;
}

bool KeyPressEventFilter::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() != QEvent::KeyPress)
        return QObject::eventFilter(obj, event);

    QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
    switch(keyEvent->key())
    {
        case Qt::Key_Escape:
        {
            if (!inPauseMenu)
            {
                inPauseMenu = true;

                Pause *p = new Pause();
                p->exec();

                inPauseMenu = false;
            }

            break;
        }
    }

    return true;
}
