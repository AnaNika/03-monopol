#include "../include/game_over.h"
#include "../build/ui_game_over.h"


void Game_over::set_frame(){
    QPixmap game_over_bkgnd = QPixmap(":/images/stripped_background.jpeg");
    game_over_bkgnd = game_over_bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Window, game_over_bkgnd);
    this->setPalette(palette);
    frame = new QFrame(this);
    frame->setGeometry((this->size().width()-border.width())/2, (this->size().height() - border.height())/2, border.width(), border.height());
    frame->setAutoFillBackground(true);
    frame->show();
}
void Game_over::set_score(QVBoxLayout &vlayout, std::vector<std::pair<QString, int>> score){

    QLabel *lplayer_score = new QLabel("Winner: " + score[0].first + ". Congrats!", frame);
    lplayer_score->setAlignment(Qt::AlignHCenter);
    QFont font("URW Bookman L", 15);
    lplayer_score->setStyleSheet("color: #23b716;"
                        "image: no-image;");
    lplayer_score->setFont(font);
    vlayout.addWidget(lplayer_score);

    for (auto p:score){
        QString text = QString::fromStdString(p.first.toStdString() + ": " + std::to_string(p.second));
        std::cout<<text.toStdString()<<std::endl;
        QLabel *lplayer_score = new QLabel(text, frame);
        lplayer_score->setAlignment(Qt::AlignHCenter);
        QFont font("URW Bookman L", 15);
        lplayer_score->setStyleSheet("color: #b6b6b6;"
                            "image: no-image;");
        lplayer_score->setFont(font);
        vlayout.addWidget(lplayer_score);
    }


}
void Game_over::add_buttons(QVBoxLayout &vlayout, QHBoxLayout &hlayout){

    vlayout.addSpacerItem(new QSpacerItem(0, border_height/10, QSizePolicy::Minimum, QSizePolicy::Minimum));

    exit = new QPushButton("EXIT", frame);
    exit->setStyleSheet("image : no-image;"
                        "background-color: #b6b6b6;"
                        "font-family : URW Bookman L;");
    exit->setFixedWidth(100);
    exit->setFixedHeight(50);

    hlayout.addSpacerItem(new QSpacerItem(border_width/5, 0, QSizePolicy::Minimum, QSizePolicy::Minimum));
    hlayout.addWidget(exit);
    hlayout.addSpacerItem(new QSpacerItem(border_width/5, 0, QSizePolicy::Minimum, QSizePolicy::Minimum));
}

void Game_over::set_content(std::vector<std::pair<QString, int> > score){
    QPalette palette;
    border = border.scaled(frame->size(), Qt::IgnoreAspectRatio);
    palette.setBrush(QPalette::WindowText, border);
    frame->setStyleSheet("image: url(:/images/chalkboard.jpg);");

    QVBoxLayout *vlayout = new QVBoxLayout(frame);
    QLabel *text = new QLabel("GAME OVER", frame);
    QFont font("URW Bookman L", 40);
    text->setStyleSheet("color: #b6b6b6;"
                        "image: no-image;");
    text->setFont(font);
    vlayout->addWidget(text);
    vlayout->addSpacerItem(new QSpacerItem(0, 20, QSizePolicy::Minimum, QSizePolicy::Minimum));
    text->setAlignment(Qt::AlignHCenter);

    set_score(*vlayout, score);

    QHBoxLayout *hlayout = new QHBoxLayout();
    vlayout->addLayout(hlayout);

    add_buttons(*vlayout, *hlayout);
}
Game_over::Game_over(std::vector<std::pair<QString, int> > score, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Game_over)
{

    set_frame();
    set_content(score);
    connect(exit, &QPushButton::clicked, this, &Game_over::exit_clicked);
    ui->setupUi(this);

}
void Game_over::resizeEvent(QResizeEvent *event)
{
    frame->move((this->size().width()-border.width())/2, (this->size().height() - border.height())/2);
    QWidget::resizeEvent(event);
}
Game_over::~Game_over()
{
    delete frame;
    delete ui;
}

void Game_over::exit_clicked(){
    QApplication::quit();
}

